'use strict';

var da_app = angular.module('daApp',
    [
        'ngAnimate',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'daServices',
        'daControllers',
        'daDirectives',
        'landingDirectives',
        'daFilters',
        'daCfg',
        'daTabor',
        'ui.bootstrap',
        'ui.utils',
        'timer',
        'ui.codemirror',
        'ngMask',
        'yaMap',
        'ngFileUpload',
        'ngClipboard'
    ],
    function ($interpolateProvider, $resourceProvider, ngClipProvider, $locationProvider) {
        //$interpolateProvider.startSymbol('{?');
        //$interpolateProvider.endSymbol('?}');
        $resourceProvider.defaults.stripTrailingSlashes = false;
        ngClipProvider.setPath("/static/js/zeroclipboard-2.2.0/dist/ZeroClipboard.swf");
        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});
    });

angular.module('daCfg', [])
    .constant('cfg', {
        'API_URL': 'http://mcu.sweetdrinks.ru/callback/index.php/api2/',
        'PORT': ':8000'
    });